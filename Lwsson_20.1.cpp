#include <iostream>
#include <string>

enum class Result
{
	SUCCESS,
	WORK,
	ERROR_READ,
	ERROR_WRITE
};

struct Bag
{
	std::string books[];
};

struct Person
{
	int Age = 0;
	int Height = 0;
	std::string Name = "Nick";
	//Bag myBag;

	void GetInfo()
	{
		std::cout << "Age: " << Age << "\n";
		std::cout << "Height: " << Height << "\n";
		std::cout << "Name: " << Name << "\n";
	}
};


int main()
{
	Result myResult = Result::WORK;
	std::cout << static_cast<int>(myResult) << "\n";
	std::cout << static_cast<int>(Result::ERROR_READ) << "\n";

	Person* Me1 = new Person{ 29, 183, "Ilya" };
	Me1->GetInfo();


	return 0;
}